import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MissionsListComponent } from './components/missions-list/missions-list.component';
import { LeafletMapComponent } from './components/leaflet-map/leaflet-map.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { CreateMissionComponent } from './components/create-mission/create-mission.component';
import { FleetsListComponent } from './components/fleets-list/fleets-list.component';
import { MissionDetailsComponent } from './components/mission-details/mission-details.component';
import { DroneDetailsComponent } from './components/drone-details/drone-details.component';
import { FleetDetailsComponent } from './components/fleet-details/fleet-details.component';

@NgModule({
  declarations: [
    AppComponent,
    MissionsListComponent,
    HomepageComponent,
    LeafletMapComponent,
    CreateMissionComponent,
    FleetsListComponent,
    MissionDetailsComponent,
    DroneDetailsComponent,
    FleetDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
