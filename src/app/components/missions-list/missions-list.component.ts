import { Component, OnInit } from '@angular/core';
import { MissionService } from '../../services/mission/mission.service';
import { Mission } from '../../shared/mission';

@Component({
  selector: 'missions-list',
  templateUrl: './missions-list.component.html',
  styleUrls: ['./missions-list.component.css'],
  providers: [MissionService]
})
export class MissionsListComponent implements OnInit {
  missions: Mission[];
  constructor(private missionService: MissionService) { }

  ngOnInit() {
      // Get the missions.
      this.missions = this.missionService.getMissions();
  }

}
