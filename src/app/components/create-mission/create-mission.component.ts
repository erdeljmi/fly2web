import { Component, OnInit } from '@angular/core';
import { NgForm }            from '@angular/forms';
import { MissionService } from '../../services/mission/mission.service';
import { FleetService } from '../../services/fleet/fleet.service';
import { Mission, WaypointMission, SurveillanceMission, MappingMission } from '../../shared/mission';
import { Fleet } from '../../shared/fleet';

@Component({
  selector: 'create-mission',
  templateUrl: './create-mission.component.html',
  styleUrls: ['./create-mission.component.css'],
  providers: [MissionService, FleetService]
})
export class CreateMissionComponent implements OnInit {

  // The mission that's being created.
  newMission: Mission;

  // Messages.
  error: string;
  info: string;

  // Fleets.
  fleets: Fleet[];

  constructor(
      private missionService: MissionService,
      private fleetService: FleetService
  ) { }

  ngOnInit() {
      // Get the list of the available fleets.
      this.refreshFleets();
  }

  // When the form is submitted.
  onSubmit(f: NgForm): void {
      // Reset the messages;
      this.error = "";
      this.info = "";
      // Check the type and create the mission.
      switch(f.value.type) {
          case "waypoint":
            this.newMission = new WaypointMission();
            break;

          case "surveillance":
            this.newMission = new SurveillanceMission();
            break;

          case "mapping":
            this.newMission = new MappingMission();
            break;

          default:
            // If the type is not set.
            this.error = "Mission type must be selected!";
            return;
      }

      // If the name is empty.
      if(f.value.name == null || f.value.name == "") {
          this.error = "Mission name must be set!";
          return;
      }

      // If the fleet is empty.
      if(f.value.fleet == null || f.value.fleet == "") {
          this.error = "A fleet must be chosen!";
          return;
      }

      // Set the data of the mission.
      this.newMission.setStarted(true);
      this.newMission.setTitle(f.value.name);
      this.newMission.setFleetId(f.value.fleet);

      // Update the state of the fleet.
      this.fleetService.putFleetUnavailable(f.value.fleet);

      // POST the mission.
      this.missionService.postMission(this.newMission);

      // Info message.
      this.info = "Mission added with success!";
      // Update the input of the fleets.
      this.refreshFleets();
      f.reset();
  }

  // Update the list of available fleets.
  refreshFleets(): void {
      this.fleets = this.fleetService.getFleets().filter(function(fleet) {
          return fleet.isAvailable();
      });
  }

}
