import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LeafletMapComponent } from "../leaflet-map/leaflet-map.component";
import { MissionsListComponent } from "../missions-list/missions-list.component";
import { FleetsListComponent } from "../fleets-list/fleets-list.component";
import { FormsModule } from '@angular/forms';
import { CreateMissionComponent } from './create-mission.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('CreateMissionComponent', () => {
  let component: CreateMissionComponent;
  let fixture: ComponentFixture<CreateMissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, RouterTestingModule ],
      declarations: [
          CreateMissionComponent,
          LeafletMapComponent,
          MissionsListComponent,
          FleetsListComponent
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
