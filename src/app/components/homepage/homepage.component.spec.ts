import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageComponent } from './homepage.component';
import { LeafletMapComponent } from "../leaflet-map/leaflet-map.component";
import { MissionsListComponent } from "../missions-list/missions-list.component";
import { FleetsListComponent } from "../fleets-list/fleets-list.component";
import { MissionDetailsComponent } from "../mission-details/mission-details.component";
import { RouterTestingModule } from '@angular/router/testing';

describe('HomepageComponent', () => {
  let component: HomepageComponent;
  let fixture: ComponentFixture<HomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [ RouterTestingModule ],
      declarations: [
          HomepageComponent,
          LeafletMapComponent,
          MissionsListComponent,
          FleetsListComponent,
          MissionsListComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
