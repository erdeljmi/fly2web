import { Component, OnInit } from '@angular/core';
import { Fleet } from '../../shared/fleet';
import { FleetService } from '../../services/fleet/fleet.service';

@Component({
  selector: 'fleets-list',
  templateUrl: './fleets-list.component.html',
  styleUrls: ['./fleets-list.component.css'],
  providers: [FleetService]
})
export class FleetsListComponent implements OnInit {
  fleets: Fleet[];
  constructor(private fleetService: FleetService) { }

  ngOnInit() {
      // Get the fleets.
      this.fleets = this.fleetService.getFleets();
  }

}
