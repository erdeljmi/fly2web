import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Mission } from '../../shared/mission';
import { Fleet } from '../../shared/fleet';
import { NgForm }            from '@angular/forms';
import { MissionService } from '../../services/mission/mission.service';
import { FleetService } from '../../services/fleet/fleet.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'mission-details',
  templateUrl: './mission-details.component.html',
  styleUrls: ['./mission-details.component.css'],
  providers: [MissionService, FleetService]
})
export class MissionDetailsComponent implements OnInit {

  // Selected mission
  selectedMission: Mission;
  missionFleet: Fleet;

  // Messages
  error: string;
  info: string;

  // For the parameter
  id: number;

  constructor(
    private missionService: MissionService,
    private fleetService: FleetService,
    private route: ActivatedRoute,
    private router: Router
  ) {
      // If we just change mission (different id) the ngOnInit doesn't run.
      // Get the parameter
      this.route.params.subscribe(params => {
         this.id = +params['id']; // (+) converts string 'id' to a number

         // In a real app: dispatch action to load the details here.
         // GET the mission.
         this.selectedMission = this.missionService.getMission(this.id);
         // Redirect if the mission does not exist.
         if (this.selectedMission != null) {
             // GET the fleet.
             if (this.selectedMission.getFleetId() != null) {
                 this.missionFleet = this.fleetService.getFleet(this.selectedMission.getFleetId());
             }
         } else {
             this.router.navigate(['/home']);
         }
      });
  }

  ngOnInit() {

  }
}
