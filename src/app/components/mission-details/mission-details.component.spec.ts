import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgModel } from '@angular/forms';

import { MissionsListComponent } from "../missions-list/missions-list.component";
import { HomepageComponent } from "../homepage/homepage.component";
import { LeafletMapComponent } from "../leaflet-map/leaflet-map.component";
import { MissionDetailsComponent } from './mission-details.component';
import { FleetsListComponent } from "../fleets-list/fleets-list.component";
import { RouterTestingModule } from '@angular/router/testing';

describe('MissionDetailsComponent', () => {
  let component: MissionDetailsComponent;
  let fixture: ComponentFixture<MissionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [ RouterTestingModule ],
      declarations: [
          MissionDetailsComponent,
          NgModel,
          LeafletMapComponent,
          FleetsListComponent,
          MissionsListComponent
       ]
    })
    .compileComponents();
    RouterTestingModule.withRoutes([
        { path: '/home', component: HomepageComponent}
    ])
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
