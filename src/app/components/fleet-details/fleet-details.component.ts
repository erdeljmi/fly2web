import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Fleet } from '../../shared/fleet';
import { FleetService } from '../../services/fleet/fleet.service';
import { DroneService } from '../../services/drone/drone.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'fleet-details',
  templateUrl: './fleet-details.component.html',
  styleUrls: ['./fleet-details.component.css'],
  providers: [FleetService, DroneService]
})
export class FleetDetailsComponent implements OnInit {

    // Selected fleet.
    selectedFleet: Fleet;

    // Messages
    error: string;
    info: string;
    logs: Object[];

    // For the parameter
    id: number;

    constructor(
      private fleetService: FleetService,
      private droneService: DroneService,
      private route: ActivatedRoute,
      private router: Router
    ) {
        this.logs = [];
        // If we just change fleet (different id) the ngOnInit doesn't run.
        // Get the parameter
        this.route.params.subscribe(params => {
           this.id = +params['id']; // (+) converts string 'id' to a number

           // In a real app: dispatch action to load the details here.
           // GET the fleet.
           this.selectedFleet = this.fleetService.getFleet(this.id);
           // Redirect if the mission does not exist.
           if (this.selectedFleet == null) {
               this.router.navigate(['/home']);
           }
        });
    }

    ngOnInit() {

    }

    // Log actions in the textarea.
    log(msg: String, error?: boolean): void {
        // Add the date.
        var currentDate: Date;
        currentDate = new Date();
        msg = currentDate.getFullYear()
            + "-" + currentDate.getMonth()
            + "-" + currentDate.getDay()
            + " " + currentDate.getHours()
            + ":" + currentDate.getMinutes()
            + ":" + currentDate.getSeconds()
            + ": " + msg;
        // Add the new message.
        if (error === true) {
            this.logs.unshift({
                message: msg,
                class: 'red'
            });
        } else if (error === false) {
            this.logs.unshift({
                message: msg,
                class: 'green'
            });
        } else {
            this.logs.unshift({
                message: msg,
                class: ''
            });
        }

        console.log(this.logs);
    }

    // Click on button "emergency land"
    emergencyLand(): void {
        this.log("REQUEST: emergency land for fleet #" + this.id);
        // For the demo, it calls drone telemetry.
        // TODO: replace this line!
        this.droneService
            .getTelemetry("1", "1")
            // When the response arrives.
            .then(data => {
                // OK
                console.log(data);
                this.log("RESPONSE: fleet #" + this.id + " landed successfully.", false);
            })
            .catch(error => {
                // NOT OK.
                console.log(error);
                this.log("RESPONSE: fleet #" + this.id + " failed to land.", true)
            });
    }

    // Click on button "ring"
    ring(): void {
        console.log("ring");
    }

    // Click on button "stack"
    stack(): void {
        console.log("stack");
    }

    // Click on button "column"
    column(): void {
        console.log("column");
    }

    // Click on button "line"
    line(): void {
        console.log("line");
    }

    // Click on button "takeoff"
    takeoff(): void {
        console.log("takeoff");
        this.log("REQUEST: Takeoff for fleet #" + this.id);
        this.fleetService
            .takeOff(this.id.toString())
            // When the response arrives.
            .then(data => {
                // OK
                console.log(data);
                this.log("RESPONSE: fleet #" + this.id + " took off successfully.", false);
            })
            .catch(error => {
                // NOT OK.
                console.log(error);
                this.log("RESPONSE: fleet #" + this.id + " failed to takeOff.", true)
            });

    }

    // Click on button "land"
    land(): void {
        console.log("land");
    }

    // Click on button "update target"
    updateTarget(): void {
        console.log("update target");
    }

    // Click on button "start" in target control
    targetStart(): void {
        console.log("start");
    }

    // Click on button "elect"
    elect(): void {
        console.log("elect");
    }

    // Click on button "replace"
    replace(): void {
        console.log("replace");
    }

    // Click on button "start" in mission Control
    missionStart(): void {
        console.log("start mission");
    }

    // Click on button "hold"
    hold(): void {
        console.log("hold");
    }
}
