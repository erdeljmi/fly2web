import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsListComponent } from "../missions-list/missions-list.component";
import { HomepageComponent } from "../homepage/homepage.component";
import { LeafletMapComponent } from "../leaflet-map/leaflet-map.component";
import { FleetsListComponent } from "../fleets-list/fleets-list.component";
import { RouterTestingModule } from '@angular/router/testing';
import { DroneDetailsComponent } from './drone-details.component';

describe('DroneDetailsComponent', () => {
  let component: DroneDetailsComponent;
  let fixture: ComponentFixture<DroneDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [ RouterTestingModule ],
        declarations: [
            DroneDetailsComponent,
            LeafletMapComponent,
            FleetsListComponent,
            MissionsListComponent
        ]
    })
    .compileComponents();
    RouterTestingModule.withRoutes([
        { path: '/home', component: HomepageComponent}
    ])
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DroneDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
