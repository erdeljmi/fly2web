import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Drone } from '../../shared/drone';
import { DroneService } from '../../services/drone/drone.service';
import { MapService } from '../../services/map/map.service';
import * as JSMpeg from 'jsmpeg';

@Component({
  selector: 'app-drone-details',
  templateUrl: './drone-details.component.html',
  styleUrls: ['./drone-details.component.css'],
  providers: [ DroneService, MapService ]
})
export class DroneDetailsComponent implements OnInit {

    // Selected drone.
    selectedDrone: Drone;

    // Messages.
    error: string;
    info: string;

    // For the parameter.
    id: number;

    // telemetry
    telemetry: Object;
    // Streaming url.
    streamingUrl: string;

    constructor(
      private droneService: DroneService,
      private mapService: MapService,
      private route: ActivatedRoute,
      private router: Router,
      private location: Location
    ) {
        // If we just change drone (different id) the ngOnInit doesn't run.
        // Get the parameter
        this.route.params.subscribe(params => {
           this.id = +params['id']; // (+) converts string 'id' to a number

           // GET the drone.
           this.selectedDrone = this.droneService.getDrone(this.id);
           // Redirect if the drone does not exist.
           if (this.selectedDrone == null) {
                this.router.navigate(['/home']);
           }

           // Display the drone on the map.
           this.displayDrone();

           // Display the streaming of the drone.
           this.displayDroneStreaming();

        });

    }

  ngOnInit() {
  }

  // Click on the "X" button.
  onClose(): void {
      this.location.back();
  }

  // Display the drone position on the map.
  displayDrone(): void {
      // GET the telemetry.
      this.droneService
          .getTelemetry("1", this.id.toString())
          // When the response arrives.
          .then(data => {
              // OK
              // Parse the result into a JSON object.
              this.telemetry = JSON.parse(data.result);

              // TODO: add marker on Leaflet map.
              this.mapService.update(this.id, this.telemetry);

              // For continuous update
              this.displayDrone();

          })
          .catch(error => {
              // NOT OK.
              console.log(error);
          });
  }

  // Display the drone streaming.
  displayDroneStreaming(): void {
      // First try.
      if (!this.streamingUrl) {
          // GET the streaming url.
          this.droneService
          .getStreaming(this.id.toString())
          // When the response arrives.
          .then(data => {
              // OK
              // Parse the result into a JSON object.
              this.streamingUrl = JSON.parse(data.result).url;

              // Display the video.
              var canvas = document.getElementById('video-canvas');
              new JSMpeg(this.streamingUrl, {canvas: canvas});

          })
          .catch(error => {
              // NOT OK.
              console.log(error);
          });
      } else {
          // Display the video.
          var canvas = document.getElementById('video-canvas');
          new JSMpeg.Player(this.streamingUrl, {canvas: canvas});
      }
  }

}
