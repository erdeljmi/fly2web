import { Component, OnInit, AfterViewInit  } from '@angular/core';

import { MapService } from '../../services/map/map.service';

@Component({
  selector: 'leaflet-map',
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.css'],
  providers: [MapService]
})
export class LeafletMapComponent implements OnInit {

  constructor(private mapService: MapService) { }

  gpx: any;

  ngOnInit() {
  }

  ngAfterViewInit() {
      this.mapService.plotActivity();
  }

}
