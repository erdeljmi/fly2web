import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from '../components/homepage/homepage.component';
import { CreateMissionComponent } from '../components/create-mission/create-mission.component';
import { MissionDetailsComponent } from '../components/mission-details/mission-details.component';
import { DroneDetailsComponent } from '../components/drone-details/drone-details.component';
import { FleetDetailsComponent } from '../components/fleet-details/fleet-details.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home',  component: HomepageComponent },
    { path: 'mission/new',  component: CreateMissionComponent },
    { path: 'mission/:id',  component: MissionDetailsComponent },
    { path: 'drone/:id',  component: DroneDetailsComponent },
    { path: 'fleet/:id',  component: FleetDetailsComponent },
    // Any other URLs.
    { path: '**',  redirectTo: '/home' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
