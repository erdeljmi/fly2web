import { Injectable } from '@angular/core';
import { Fleet } from '../../shared/fleet';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { mockFleets } from '../../shared/fleets-mock';

const API_URL = "http://localhost:3000/";

@Injectable()
export class FleetService {

  fleets: Fleet[];

  // Initialize with a few fleets.
  constructor(private http: HttpClient) {
      this.fleets = mockFleets;
      this.http = http;
  }

  // Returns the fleets.
  getFleets(): Fleet[] {
      return this.fleets;
  }

  // Returns a fleet from id
  getFleet(p_id: number): Fleet {
      for (let fleet of this.fleets) {
        if (fleet.getId() == p_id){
          return fleet;
        }
      }
  }

  // Update a fleet.
  putFleetUnavailable(pFleetId: number): void {
      for (let fleet of this.fleets) {
          if (fleet.getId() == pFleetId) {
              fleet.setAvailable(false);
          }
      }
  }

  // Emergency land.
  emergencyLand(pId: string): Promise<any> {
      var query: String;

      // random number to test both OK and NOT OK queries.
      if (Math.floor(Math.random() * 2)) {
          // OK.
          query = "fleets";
      } else {
          // FAIL.
          query = "FAIL";
      }

      return this.http
        // TODO: replace with a POST request.
        .get(API_URL + query, {
            // request params.
            params: new HttpParams().set('id', pId)
        })
        .toPromise();
  }

  // Takeoff.
  takeOff(pId: string): Promise<any> {
    var query: String;

    // random number to test both OK and NOT OK queries.
    if (Math.floor(Math.random() * 2)) {
        // OK.
        query = "fleets";
    } else {
        // FAIL.
        query = "FAIL";
    }

    return this.http
    // TODO: replace with a POST request.
    .get(API_URL + query, {
      // reques params.
      params: new HttpParams().set('id', pId)
    })
    .toPromise();
  }
}
