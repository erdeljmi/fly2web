import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as L from 'leaflet';
import { Map } from 'leaflet';

const defaultCoords: number[] = [40, -80]
const defaultZoom: number = 8

var apiToken = environment.MAPBOX_API_KEY;
declare var omnivore: any;

declare global {
    interface Window { myMap : Map; marker : any; }
}

@Injectable()
export class MapService {

    constructor() {}

    initMap(): void{
        if (window.myMap == null){
            window.myMap = new L.map('map').setView(defaultCoords, defaultZoom);

            window.myMap.maxZoom = 100;

            L.tileLayer('https://api.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets-basic',
                accessToken: apiToken
            }).addTo(window.myMap);
        }
    }

    plotActivity(): void {
        this.initMap();

        var myStyle = {
            "color": "#3949AB",
            "weight": 5,
            "opacity": 0.95
        };

        var customLayer = L.geoJson(null, {
            style: myStyle
        });


        /*var gpxLayer = omnivore.gpx("../../../assets/gpx/gps1.gpx", null, customLayer)
            .on('ready', function() {
            window.myMap.fitBounds(gpxLayer.getBounds());
        }).addTo(window.myMap);*/
    }

    update (p_id: number, p_telemetry: any): void {
        //this.initMap();

        var UAV_icon = L.icon({
            iconUrl: '../../../assets/images/UAV_icon.png',

            iconSize:  [30, 30],
            iconAnchor: [15, 15],
        })

        // Marker creation
        if (!window.marker){
            window.marker = L.marker([p_telemetry.latitude, p_telemetry.longitude], {icon: UAV_icon}).addTo(window.myMap);
        }
        // Marker update
        else{
            window.marker.setLatLng([p_telemetry.latitude, p_telemetry.longitude], {icon: UAV_icon}).update();
        }
    }
}
