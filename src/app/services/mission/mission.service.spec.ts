import { TestBed, inject } from '@angular/core/testing';

import { MissionService } from './mission.service';

describe('MissionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MissionService]
    });
  });

  it('should be created', inject([MissionService], (service: MissionService) => {
    expect(service).toBeTruthy();
  }));

  it('should get 9 missions', inject([MissionService], (service: MissionService) => {
    expect(service.getMissions().length).toEqual(9);
  }));

  it('should display the mission name', inject([MissionService], (service: MissionService) => {
    expect(service.getMissions()[0].getName()).toEqual("9# the mapping mission");
  }));
});
