import { Injectable } from '@angular/core';
import { Mission } from '../../shared/mission';
import { mockMissions } from '../../shared/missions-mock';

@Injectable()
export class MissionService {

  missions: Mission[];

  // Initialize with a few missions.
  constructor() {
      this.missions = mockMissions;
  }

  // Returns the missions.
  getMissions(): Mission[] {
      // Sort the missions.
      this.sortMissions();
      return this.missions;
  }

  // Returns a mission from id
  getMission(p_id: number): Mission {
      for (let mission of this.missions) {
        if (mission.getId() == p_id){
          return mission;
        }
      }
  }

  // Sort the missions.
  sortMissions(): void {
      this.missions.sort(function(missionA: Mission, missionB: Mission) {
          if (missionA.getId() > missionB.getId()) {
              return -1;
          } else {
              return 1;
          }
      });
  }

  // POST one mission.
  postMission(pMission: Mission): void {
      // The new id.
      let newId;
      if (this.missions.length > 0) {
          newId = this.missions[0].getId() + 1;
      } else {
          newId = 1;
      }
      pMission.setId(newId);

      // Add the mission to the list.
      this.missions.unshift(pMission);
  }

  // PUT a mission.
  updateMission(p_mission: Mission): void {
      let missionToDelete: Mission;
      let index: number;

      missionToDelete = this.missions.find(mission => mission.getId() === p_mission.getId());
      delete this.missions[this.missions.indexOf(missionToDelete, 0)];

      // Add the mission to the list.
      this.missions.unshift(p_mission);

      // Sort the list
      this.sortMissions();
  }

}
