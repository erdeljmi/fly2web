import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Drone } from '../../shared/drone';
import { mockDrones } from '../../shared/drones-mock';

const API_URL = "http://localhost:8080/";

@Injectable()
export class DroneService {

    drones: Drone[];

    // Initialize with a few drones.
    constructor(private http: HttpClient) {
        this.drones = mockDrones;
        this.http = http;
    }

    // Returns the drones.
    getDrones(): Drone[] {
        return this.drones;
    }

    // Returns a drone from id
    getDrone(p_id: number): Drone {
        for (let drone of this.drones) {
          if (drone.getId() == p_id){
            return drone;
          }
        }
    }

    // Get the telemetry of a drone.
    getTelemetry(pUavServerId: string, pDroneId: string): Promise<any> {

        return this.http
          .get(API_URL + "drones_telemetry/" + pUavServerId + "/" + pDroneId)
          .toPromise();
    }

    // Get the streaming link of a drone.
    getStreaming(pDroneId: string): Promise<any> {

        return this.http
          .get(API_URL + "drones_streaming/" + pDroneId)
          .toPromise();
    }
}
