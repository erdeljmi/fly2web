export class Mission {
    id: number;
    title: string;
    started: boolean;
    over: boolean;
    fleetId: number; // Or the whole Object fleet ?

    // Constructor of a mission.
    constructor(pId?: number, pTitle?: string, pStarted?: boolean, pFleetId?: number) {
        this.id = pId;
        this.title = pTitle;
        this.started = pStarted;
        this.fleetId = pFleetId;
        this.over = false;
    }

    // Return the name and the id of the mission.
    getName(): string {
        return this.id + "# " + this.title;
    }

    // Return the title.
    getTitle(): string {
        return this.title;
    }

    // Set the title.
    setTitle(pTitle: string): void {
        this.title = pTitle;
    }

    // Return the id.
    getId(): number {
        return this.id;
    }

    // Set the id.
    setId(pId: number): void {
        this.id = pId;
    }

    // Return the fleet id.
    getFleetId(): number {
        return this.fleetId;
    }

    // Set the fleet id.
    setFleetId(pFleetId: number): void {
        this.fleetId = pFleetId;
    }

    // Change the state of a mission.
    setStarted(pState: boolean) {
        this.started = pState;
    }

    // Return true if the mission is started.
    isStarted(): boolean {
        return this.started;
    }

    // Change the state of a mission.
    setOver(pState: boolean) {
        this.over = pState;
    }

    // Return true if the mission is over.
    isOver(): boolean {
        return this.over;
    }

}

export class WaypointMission extends Mission {

    // Constructor of a waypoint mission.
    constructor(pId?: number, pTitle?: string, pStarted?: boolean, pFleetId?: number) {
        super(pId, pTitle, pStarted, pFleetId);
    }

    // Return the CSS class to use.
    getClass(): string {
        if (this.over) {
            return "mission-item-waypoint list-group-item-warning";
        } else {
            if (this.started) {
                return "mission-item-waypoint list-group-item-success";
            } else {
                return "mission-item-waypoint list-group-item-primary";
            }
        }
    }

}

export class SurveillanceMission extends Mission {

    // Constructor of a surveillance mission.
    constructor(pId?: number, pTitle?: string, pStarted?: boolean, pFleetId?: number) {
        super(pId, pTitle, pStarted, pFleetId);
    }

    // Return the CSS class to use.
    getClass(): string {
        if (this.over) {
            return "mission-item-surveillance list-group-item-warning";
        } else {
            if (this.started) {
                return "mission-item-surveillance list-group-item-success";
            } else {
                return "mission-item-surveillance list-group-item-primary";
            }
        }
    }

}

export class MappingMission extends Mission {

    // Constructor of a mapping mission.
    constructor(pId?: number, pTitle?: string, pStarted?: boolean, pFleetId?: number) {
        super(pId, pTitle, pStarted, pFleetId);
    }

    // Return the CSS class to use.
    getClass(): string {
        if (this.over) {
            return "mission-item-mapping list-group-item-warning";
        } else {
            if (this.started) {
                return "mission-item-mapping list-group-item-success";
            } else {
                return "mission-item-mapping list-group-item-primary";
            }
        }
    }

}
