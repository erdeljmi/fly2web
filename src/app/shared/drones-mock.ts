import { Drone } from './drone';

export var mockDrones: Drone[] = [
    new Drone(1),
    new Drone(2),
    new Drone(3),
    new Drone(4),
    new Drone(5),
    new Drone(6),
    new Drone(7),
    new Drone(8),
    new Drone(9),
    new Drone(10),
]
