import { Fleet } from './fleet';
import { Drone } from './drone';

export var mockFleets: Fleet[] = [
    new Fleet(1, [new Drone(1), new Drone(2), new Drone(3)], true),
    new Fleet(2, [new Drone(4), new Drone(5)], false),
    new Fleet(3, [new Drone(6), new Drone(7), new Drone(8), new Drone(9)], true),
    new Fleet(4, [new Drone(10)], true),
]
