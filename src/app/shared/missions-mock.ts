import { Mission, WaypointMission, SurveillanceMission, MappingMission } from './mission';

export var mockMissions: Mission[] = [
    new WaypointMission(1, "the waypoint mission", true, 1),
    new SurveillanceMission(2, "the surveillance mission", false, 2),
    new MappingMission(3, "the mapping mission", true, 3),
    new WaypointMission(4, "the waypoint mission", true, 4),
    new SurveillanceMission(5, "the surveillance mission", false, 1),
    new MappingMission(6, "the mapping mission", false, 2),
    new WaypointMission(7, "the waypoint mission", false, 3),
    new SurveillanceMission(8, "the surveillance mission", true, 4),
    new MappingMission(9, "the mapping mission", false, 1),
]
