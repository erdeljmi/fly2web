export class Drone {
    id: number;

    // Constructor of a drone.
    constructor(pId?: number) {
        this.id = pId;
    }

    // Return the id.
    getId(): number {
        return this.id;
    }

    // Returns the name.
    getName(): string {
        return "Drone #" + this.id;
    }
}
