import { Drone } from './drone';

export class Fleet {
    id: number;
    drones: Drone[];
    available: boolean;

    // Constructor of a fleet.
    constructor(pId?: number, pDrones?: Drone[], pAvailabe?: boolean) {
        this.id = pId;
        this.drones = pDrones;
        this.available = pAvailabe;
    }

    // Return the id.
    getId(): number {
        return this.id;
    }

    // Get the list of drones.
    getDrones(): Drone[] {
        return this.drones;
    }

    // Get the number of drones.
    getSize(): number {
        return this.drones.length;
    }

    // Get the "name".
    getName(): string {
        let name = "Fleet #" + this.id;
        if (this.getSize() == 1) {
            name += " (1 drone)";
        } else {
            name += " ("+ this.getSize() +" drones)"
        }

        return name;
    }

    // Return a css class if a mission is set.
    getClass(): any {
        if (!this.isAvailable()) {
            return "list-group-item-secondary";
        }
    }

    // Returns "true" if the fleet is available.
    isAvailable(): boolean {
        return this.available;
    }

    // Set the state of the fleet.
    setAvailable(pAvailable: boolean): void {
        this.available = pAvailable;
    }
}
