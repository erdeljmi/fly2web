import { AppPage } from './app.po';

describe('uav-fleet-control App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('UAV Fleet Control');
  });
});
